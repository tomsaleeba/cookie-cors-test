This is a test rig to see how cross-origin cookies work.

You can make a `fetch()` call with `credentials: include` and make sure that
the server responds with all the required headers so it's ok. However, it
wasn't apparent *which* cookies would be sent cross domain. Is it the ones from
the current domain, or the domain that matches the destination of the
cross-origin request.

At this stage, I'm fairly sure it's the latter. This demonstrates that.

## TODO
- add a HTML view/templating engine so it's easier to write a page that has
    JS in it to make cross origin `fetch` calls
- add required CORS support to server
