const Koa = require('koa')
const router = require('koa-router')

const app = new Koa()
const routes = router()
const port = process.env.PORT || 3000

const cookieKey = 'the_cookie'

routes.get('/', ctx => {
  const msg = ctx.query.msg || ''
  ctx.body = `<body>
      <p style="color: red;">${msg}</p>
      <ul>
        <li><a href="//localhost:${port}/clear">Clear localhost cookie</a></li>
        <li><a href="//127.0.0.1:${port}/clear">Clear 127.0.0.1 cookie</a></li>
      </ul>
      <form action="/login" method="get">
        <input name="user" type="text" />
        <button type="submit">Login</button>
      </form>
    </body>`
})

routes.get('/clear', ctx => {
  const origin = ctx.origin
  console.log(`Clearing cookie for ${origin}`)
  ctx.cookies.set(cookieKey, null, {httpOnly: false})
  ctx.set('Location', '/?msg=cookie-cleared-for-' + origin)
  return ctx.status = 301
})

routes.get('/login', ctx => {
  const userId = ctx.query.user
  if (!userId) {
    ctx.status = 403
    return ctx.body = `<body>
      <p>"user" query param not supplied</p>
      <a href="/">back to login page</a>
    </body>`
  }
  const origin = ctx.origin
  console.log(`Setting cookie to ${userId} for ${origin}`)
  ctx.cookies.set(cookieKey, userId, {httpOnly: false})
  ctx.set('Location', '/home')
  return ctx.status = 301
})

routes.get('/logout', ctx => {
  ctx.cookies.set(cookieKey, null, {httpOnly: false})
  ctx.set('Location', '/')
  return ctx.status = 301
})

routes.get('/home', ctx => {
  const userId = ctx.cookies.get(cookieKey)
  if (!userId) {
    ctx.set('Location', '/')
    return ctx.status = 301
  }
  const origin = ctx.origin
  const otherOrigin = (()=>{
    if (origin.includes('locahost')) {
      return origin.replace('localhost', '127.0.0.1')
    }
    return origin.replace('127.0.0.1', 'localhost')
  })()
  ctx.body = `<body>
      <p>Welcome <strong>${userId}</strong></p>
      <div>
        The request for this image is what clones the cookie.
        <img src="${otherOrigin}/cookie-hack.jpg?user=${userId}" />
      </div>
      <p>
        <a href="//localhost:${port}/api?expected=${userId}">Hit API on localhost</a><br />
        <a href="//127.0.0.1:${port}/api?expected=${userId}">Hit API on 127.0.0.1</a>
      </p>
      <p><a href="/logout">logout</a></p>
    </body>`
})

routes.get('/cookie-hack.jpg', ctx => {
  const userId = ctx.query.user
  const origin = ctx.origin
  console.log(`Cloning cookie with value ${userId} to ${origin}`)
  ctx.cookies.set(cookieKey, userId, {httpOnly: false})
  ctx.body = {
    message: 'cookie cloned',
    value: userId
  }
})

routes.get('/api', ctx => {
  const expected = ctx.query.expected
  const userId = ctx.cookies.get(cookieKey) || '(not present)'
  if (userId !== expected) {
    ctx.status = 403
    return ctx.body = {
      success: false,
      message: 'fail: cookie auth did NOT match expected',
      cookie: userId,
      expected
    }
  }
  ctx.body = {
    success: true,
    message: 'yay, cookie matches expected',
    cookie: userId,
    expected
  }
})

app.use(routes.routes())
app.listen(port)
